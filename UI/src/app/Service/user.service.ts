import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  uri = 'http://localhost:19501/api/userDetails/SaveUserDetails'

  constructor(private http: HttpClient) { }

   insertUserdata(data:any){
   return  this.http.post(this.uri , data);
   }
}
