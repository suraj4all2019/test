import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { UserService } from '../Service/user.service';

@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.css']
})
export class UserDetailsComponent implements OnInit {
  uri = 'http://localhost:19501/api/userDetails/SaveUserDetails';
  userId =0;


  constructor(private userservice :UserService, private http : HttpClient) {
   
   }

  ngOnInit(): void {
   
  }
  InsertUser(data:any){
    console.log(data);
    //debugger
    return this.userservice.insertUserdata(data).subscribe((data=>{
      console.log(data);
    }
    
    ));
    //this.http.post(this.uri, JSON.stringify(data))

  }


}
